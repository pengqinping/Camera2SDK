package com.xgg.camera2app;

import android.content.Context;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Size;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.xgg.camera2sdk.NoPreManager;
import com.xgg.camera2sdk.PreManager;
import com.xgg.camera2sdk.PreviewCallback;
import com.xgg.camera2sdk.RecordVideoCallback;
import com.xgg.camera2sdk.TakePhotoCallback;
import java.io.File;

public class WithPreActivity extends AppCompatActivity implements View.OnClickListener , TextureView.SurfaceTextureListener {
    Context mContext;
    PreManager mManager;
    String[] mCameraList;
    int mCurCameraIndex;
    Button btn_takephoto,btn_recordvideo;
    Spinner spin_cameraid;
    TextureView pre_textureview;
    TextView txt_log;
    private Size mPreviewSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withpre);
        spin_cameraid = findViewById(R.id.spin_cameraid);
        btn_takephoto = findViewById(R.id.takephoto);
        btn_takephoto.setOnClickListener(this);
        btn_recordvideo = findViewById(R.id.record);
        btn_recordvideo.setOnClickListener(this);
        pre_textureview = findViewById(R.id.pre_textureview);
        pre_textureview.setSurfaceTextureListener(this);
        txt_log = findViewById(R.id.txt_log);

        mContext = this;
        mManager = new PreManager(this );
        File tmp = getExternalFilesDir(null);
        if (tmp != null) {
            mManager.setPhotoPath(tmp.getPath() + "/photo");
            mManager.setVideoPath(tmp.getPath() + "/video");
        }
        mManager.enableLog(mLogHandler);

        mCameraList = mManager.getCameraList();
        if (mCameraList == null){
            ShowToast(getResources().getString(R.string.no_camera_permission));
        }
        else{
            //初始化adapter
            mCurCameraIndex = 0;
            ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext , android.R.layout.simple_list_item_1 , mCameraList );
            spin_cameraid.setAdapter(adapter);
            spin_cameraid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mManager.stopPreview();
                    mCurCameraIndex = position;
                    mManager.startPreview(mCameraList[mCurCameraIndex], pre_textureview, new PreviewCallback() {
                        @Override
                        public void OnPreviewStarted() {
                        }

                        @Override
                        public void OnPreviewEnded() {
                        }

                        @Override
                        public void OnPreviewFail(int rtcode) {
                            ShowToast("OnPreviewFail:" + rtcode);
                        }
                    });
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onClick(View v){
        if (v == btn_takephoto){
            mManager.takePhoto(mCameraList[mCurCameraIndex], new TakePhotoCallback() {
                @Override
                public void OnTakePhotoSuccess(String LocalPath) {
                    ShowToast(LocalPath);
                }

                @Override
                public void OnTakePhotoFail(int rtcode) {
                    ShowToast("OnTakePhotoFail:" + rtcode);
                }
            });
        }
        else if (v == btn_recordvideo){
            mManager.recordVideo(mCameraList[mCurCameraIndex], new RecordVideoCallback() {
                @Override
                public void OnRecordVideoStarted() {

                }

                @Override
                public void OnRecordVideoEnded(String LocalPath) {

                }

                @Override
                public void OnRecordVideoFail(int rtcode) {

                }
            });
        }
    }

    void ShowToast(String string){
        Toast.makeText(this , string ,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height){
        Log.d("SurfaceTexture" , "onSurfaceTextureAvailable");

    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height){
        Log.d("SurfaceTexture" , "onSurfaceTextureSizeChanged");

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface){

        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface){
        Log.d("SurfaceTexture" , "onSurfaceTextureUpdated");

    }

    Handler mLogHandler = new Handler(){
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                String newlog = msg.obj.toString();
                String oldlog = txt_log.getText().toString();
                txt_log.setText(oldlog + "\r\n" + newlog);
            } catch (Exception e) {
            }
        }
    };
}