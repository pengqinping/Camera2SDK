package com.xgg.camera2app;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.xgg.camera2sdk.NoPreManager;
import com.xgg.camera2sdk.RecordVideoCallback;
import com.xgg.camera2sdk.TakePhotoCallback;

import java.io.File;

public class WithoutPreActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    NoPreManager mManager;
    String[] mCameraList;
    int mCurCameraIndex;
    Button btn_takephoto,btn_recordvideo;
    TextView txt_log;
    ScrollView scroll_log;

    Spinner spin_cameraid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withoutpre);
        spin_cameraid = findViewById(R.id.spin_cameraid);
        btn_takephoto = findViewById(R.id.takephoto);
        btn_takephoto.setOnClickListener(this);
        btn_recordvideo = findViewById(R.id.record);
        btn_recordvideo.setOnClickListener(this);
        txt_log = findViewById(R.id.txt_log);
        scroll_log = findViewById(R.id.scroll_log);
        mContext = this;

        mManager = new NoPreManager(this);
        mManager.enableLog(mLogHandler);
        File tmp = getExternalFilesDir(null);
        if (tmp != null) {
            mManager.setPhotoPath(tmp.getPath() + "/photo");
            mManager.setVideoPath(tmp.getPath() + "/video");
        }

        mCameraList = mManager.getCameraList();
        if (mCameraList == null){
            ShowToast(getResources().getString(R.string.no_camera_permission));
        }
        else{
            //初始化adapter
            mCurCameraIndex = 0;
            ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext , android.R.layout.simple_list_item_1 , mCameraList );
            spin_cameraid.setAdapter(adapter);
            spin_cameraid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mCurCameraIndex = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    public void onClick(View v){
        if (v == btn_takephoto){
            mManager.takePhoto(mCameraList[mCurCameraIndex], new TakePhotoCallback() {
                @Override
                public void OnTakePhotoSuccess(String LocalPath) {
                    ShowToast(LocalPath);
                }

                @Override
                public void OnTakePhotoFail(int rtcode) {
                    ShowToast("OnTakePhotoFail:" + rtcode);
                }
            });
        }
        else if (v == btn_recordvideo){
            mManager.recordVideo(mCameraList[mCurCameraIndex], new RecordVideoCallback() {
                @Override
                public void OnRecordVideoStarted() {
                    ShowToast("开始录像");
                }

                @Override
                public void OnRecordVideoEnded(String LocalPath) {
                    ShowToast(LocalPath);
                }

                @Override
                public void OnRecordVideoFail(int rtcode) {
                    ShowToast("OnRecordVideoFail:" + rtcode);
                }
            });
        }
    }

    void ShowToast(String string){
        Toast.makeText(this , string ,Toast.LENGTH_LONG).show();
    }

    Handler mLogHandler = new Handler(){
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                String newlog = msg.obj.toString();
                String oldlog = txt_log.getText().toString();
                txt_log.setText(oldlog + "\r\n" + newlog);
            } catch (Exception e) {
            }
        }
    };
}