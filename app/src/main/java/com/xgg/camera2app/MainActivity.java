package com.xgg.camera2app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.withoutpreview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent without = new Intent();
                without.setClass(MainActivity.this , WithoutPreActivity.class);
                startActivity(without);
            }
        });

        findViewById(R.id.withpreview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent without = new Intent();
                without.setClass(MainActivity.this , WithPreActivity.class);
                startActivity(without);
            }
        });
    }
}
