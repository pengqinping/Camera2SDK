package com.xgg.camera2sdk;

public interface PreviewCallback {
    void OnPreviewStarted();
    void OnPreviewEnded();
    void OnPreviewFail(int rtcode);

    int RTCODE_EXCEPTION = 1;
    int RTCODE_INIT_FAIL = 2;
    int RTCODE_OPENCAMERA_FAIL = 3;
    int RTCODE_SESSION_FAIL = 4;
    int RTCODE_CAPTURE_FAIL = 5;
    int RTCODE_SAVE_FAIL = 6;
}
